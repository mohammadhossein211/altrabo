import React from 'react';
import App from 'next/app';
import NavBar from '../Components/NavBar';
import '../Styles/style.css';
import Footer from '../Components/Footer';

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <div className='app'>
        <NavBar />
        <Component {...pageProps} />
        <Footer />
      </div>
    );
  }
}

export default MyApp;
