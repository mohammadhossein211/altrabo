import Head from 'next/head';
import Home from '../Components/Home';

export default function Index() {
  return (
    <div className='home'>
      <Home />
    </div>
  );
}
