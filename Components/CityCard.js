import React, { Component } from 'react';

const loadingDiv = (
  <div className='spinner-border my-auto' role='status'>
    <span className='sr-only'>Loading...</span>
  </div>
);
export default class CityCard extends Component {
  render() {
    let bgImg = '';
    if (!this.props.isLoading) bgImg = `url("${this.props.img}")`;
    return (
      <div
        className='CityCard col text-center p-0 d-flex align-items-end justify-content-center'
        style={{
          backgroundImage: bgImg,
        }}
      >
        {this.props.isLoading ? (
          loadingDiv
        ) : (
          <div className='CityCard-name-c'>
            <p className='CityCard-name'>{this.props.name}</p>
          </div>
        )}
      </div>
    );
  }
}
