import React, { Component } from 'react';
import CityCard from './CityCard';

const loadingDiv = (
  <div className='d-flex justify-content-center'>
    <div className='spinner-border' role='status'>
      <span className='sr-only'>Loading...</span>
    </div>
  </div>
);
export default class Tourism extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    this.setState({
      isLoading: true,
    });
    setTimeout(() => {
      this.setState({
        isLoading: false,
      });
    }, 2000);
  }
  render() {
    let i = 0;
    const citis = this.props.cities.map((c) => {
      i = i + 1;
      return (
        <CityCard
          img={c.img}
          name={c.name}
          key={i}
          isLoading={this.state.isLoading}
        />
      );
    });
    setTimeout(() => {
      this.setState({
        isLoading: false,
      });
    }, 2000);
    return (
      <div className='Tourism'>
        <h4 className='text-center Tourism-title'>{this.props.title}</h4>
        <div className='row'>
          {citis}
          <div
            className='Tourism-btn Tourism-btn-right'
            onClick={this.handleClick}
          >
            <i className='fas fa-angle-right '></i>
          </div>
          <div
            className='Tourism-btn Tourism-btn-left'
            onClick={this.handleClick}
          >
            <i className='fas fa-angle-left '></i>
          </div>
        </div>
      </div>
    );
  }
}
