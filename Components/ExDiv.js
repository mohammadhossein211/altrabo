import React, { Component } from 'react';

export default class ExDiv extends Component {
  render() {
    return (
      <div className='ExDiv col'>
        <img src={this.props.img} alt='' />
        <p className='ExDiv-text'>{this.props.text}</p>
      </div>
    );
  }
}
