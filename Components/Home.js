import React, { Component } from 'react';
import ExDivs from './ExDivs';
import Tourism from './Tourism';
import Newsletter from './Newsletter';

const infoDivs = [
  {
    img: './images/suitcase.png',
    text:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در',
  },
  {
    img: './images/globe.png',
    text:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در',
  },
  {
    img: './images/hotel.png',
    text:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در',
  },
];
const cities = [
  {
    name: 'پکن',
    img: './images/pekan.png',
  },
  {
    name: 'پکن',
    img: './images/pekan.png',
  },
  {
    name: 'پکن',
    img: './images/pekan.png',
  },
  {
    name: 'پکن',
    img: './images/pekan.png',
  },
];
const tourismTitle = 'پیشنهادهای جهانگردی آلترابو';
export default class Home extends Component {
  render() {
    return (
      <div className='Home'>
        <div className='container mb-5 pb-5'>
          <ExDivs divs={infoDivs} />
          <Tourism cities={cities} title={tourismTitle} />
          <Tourism cities={cities} title={tourismTitle} />
        </div>
        <Newsletter />
        <div className='top-footer'>
          <h4>
            !هرجای دنیا که برید <strong>آلترابو</strong> همراهتونه
          </h4>
          <div className='row'>
            <img src='./images/topfooter/1.png' alt='' className='col-2' />
            <img src='./images/topfooter/2.png' alt='' className='col-2' />
            <img src='./images/topfooter/3.png' alt='' className='col-2' />
            <img src='./images/topfooter/4.png' alt='' className='col-2' />
            <img src='./images/topfooter/5.png' alt='' className='col-2' />
            <img src='./images/topfooter/6.png' alt='' className='col-2' />
          </div>
        </div>
      </div>
    );
  }
}
