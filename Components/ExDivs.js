import React, { Component } from 'react';
import ExDiv from './ExDiv';

export default class ExDivs extends Component {
  render() {
    let i = 0;
    const divs = this.props.divs.map((d) => {
      i = i + 1;
      return <ExDiv img={d.img} text={d.text} key={i} />;
    });

    return <div className='row'>{divs}</div>;
  }
}
