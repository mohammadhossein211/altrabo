import React, { Component } from 'react';
import Button from './Button';

export default class Newsletter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmiting: false,
      email: '',
      error: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
      error: false,
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    if (this.state.email != '') {
      this.setState({
        isSubmiting: true,
      });
      setTimeout(() => {
        this.setState(
          {
            isSubmiting: false,
          },
          () => {
            alert('شما با موفقیت عضو خبرنامه ما شدید!');
          }
        );
      }, 1000);
    } else {
      this.setState({
        error: true,
      });
    }
  }
  render() {
    return (
      <div className='Newsletter'>
        <div className='container'>
          <div className='row'>
            <img src='./images/travel.png' alt='' className='col-4' />
            <div className='col text-center my-auto'>
              <h4 className='Newsletter-title'>
                با عضویت در خبرنامه آلترابو از پیشنهادات شگفت انگیز ما زودتر از
                بقیه باخبر میشین
              </h4>

              <form
                className='row mx-auto d-flex justify-content-center'
                onSubmit={this.handleSubmit}
              >
                <input
                  type='email'
                  className='Newsletter-input'
                  value={this.state.email}
                  name='email'
                  onChange={this.handleChange}
                  placeholder='آدرس ایمیلتان را وارد کنید'
                />
                <Button
                  text='عضویت در خبرنامه'
                  isLoading={this.state.isSubmiting}
                />
              </form>
              {this.state.error ? (
                <div
                  className='alert alert-danger mx-5 mt-3 Newsletter-alert'
                  role='alert'
                >
                  لطفا ایمیلی وارد کنید
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
