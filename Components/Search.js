import React, { Component } from 'react';
import Button from './Button';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '',
      dest: '',
      date: '',
      activeBtnNum: 1,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSwitch = this.handleSwitch.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }
  handleSwitch(e) {
    this.setState((st) => {
      return {
        from: st.dest,
        dest: st.from,
      };
    });
  }
  handleClick(e) {
    this.setState({
      activeBtnNum: e.target.name,
    });
  }
  render() {
    return (
      <div className='Search'>
        <div className='d-flex justify-content-center'>
          <button
            className={`Search-item ${
              this.state.activeBtnNum == 1 ? 'Search-item-active' : null
            }`}
            name='1'
            onClick={this.handleClick}
          >
            <img src='./images/airplain.png' alt='' name='1' />
            بلیت هواپیما
          </button>
          <button
            className={`Search-item ${
              this.state.activeBtnNum == 2 ? 'Search-item-active' : null
            }`}
            name='2'
            onClick={this.handleClick}
          >
            <img name='2' src='./images/airplain.png' alt='' />
            بلیت هواپیما
          </button>
          <button
            className={`Search-item ${
              this.state.activeBtnNum == 3 ? 'Search-item-active' : null
            }`}
            name='3'
            onClick={this.handleClick}
          >
            <img src='./images/airplain.png' alt='' name='3' />
            بلیت هواپیما
          </button>
          <button
            className={`Search-item ${
              this.state.activeBtnNum == 4 ? 'Search-item-active' : null
            }`}
            name='4'
            onClick={this.handleClick}
          >
            <img src='./images/airplain.png' alt='' name='4' />
            بلیت هواپیما
          </button>
        </div>
        <div className='Search-select text-right px-4 py-4 mx-4'>
          <select name='' id=''>
            <option>یک طرفه</option>
            <option>رفت و برگشت</option>
          </select>
          <select name='' id=''>
            <option>۱ نفر بزرگسال</option>
            <option>۲ نفر بزرگسال</option>
          </select>
        </div>
        <form className='Search-info row'>
          <div className='row Search-info-inputContainer'>
            <div className='Search-spanContainer'>
              <span className='Search-info-span'>
                <i className='fas fa-map-marker-alt' />
                مبدا
              </span>
            </div>
            <input
              type='text'
              value={this.state.from}
              name='from'
              onChange={this.handleChange}
            />
          </div>
          <a className=' Search-exchange' onClick={this.handleSwitch}>
            <i className='fas fa-exchange-alt' />
          </a>
          <div className='row Search-info-inputContainer mr-2'>
            <div className='Search-spanContainer'>
              <span className='Search-info-span'>
                <i className='fas fa-map-marker-alt' />
                مقصد
              </span>
            </div>
            <input
              type='text'
              value={this.state.dest}
              name='dest'
              onChange={this.handleChange}
            />
          </div>
          <div className='row Search-info-inputContainer Search-date mr-2'>
            <div className='Search-spanContainer'>
              <span className='Search-info-span'>
                <i className='fas fa-map-marker-alt' />
                تایخ
              </span>
            </div>
            <input
              type='date'
              value={this.state.date}
              name='date'
              onChange={this.handleChange}
            />
          </div>
          <Button text='جستجو' />
        </form>
      </div>
    );
  }
}
