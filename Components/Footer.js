import React, { Component } from 'react';
import Link from 'next/link';

export default class Footer extends Component {
  render() {
    return (
      <div className='Footer'>
        <div className='container'>
          <div className='Footer-socials'>
            <div className='row'>
              <a href='#' className='col'>
                <i className='fab fa-instagram' />
              </a>
              <a href='#' className='col'>
                <i className='fab fa-google' />
              </a>
              <a href='#' className='col'>
                <i className='fab fa-linkedin-in' />
              </a>
              <a href='#' className='col'>
                <i className='fab fa-twitter' />
              </a>
            </div>
          </div>
          <div className='Footer-desc'>
            <div className='row'>
              <div className='col'>
                <h6>درباره ما</h6>
                <p>
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                  استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                  مجله در
                </p>
              </div>
              <div className='col'>
                <h6>درباره ما</h6>
                <p>
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                  استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                  مجله در
                </p>
              </div>
              <div className='col Footer-links'>
                <div className='row'>
                  <Link href='/'>
                    <a className='col'>بلیت هواپیما</a>
                  </Link>
                  <Link href='/'>
                    <a className='col'>بلیت اتوبوس</a>
                  </Link>
                </div>
                <div className='row'>
                  <Link href='/'>
                    <a className='col'>بلیت قطار</a>
                  </Link>
                  <Link href='/'>
                    <a className='col'>رزرو هتل</a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className='Footer-license'>
            <p>تمامی حقوق این وبسایت متعلق به مجموعه آلترابو است</p>
          </div>
        </div>
      </div>
    );
  }
}
