import React, { Component } from 'react';

export default class Button extends Component {
  constructor(props) {
    super(props);
    const defaultProps = {
      isLoading: false,
    };
  }
  render() {
    return (
      <button type='submit' className='Newsletter-btn'>
        {this.props.isLoading ? (
          <span
            className='spinner-border spinner-border-lg'
            role='status'
            aria-hidden='true'
          ></span>
        ) : (
          <span>{this.props.text}</span>
        )}
      </button>
    );
  }
}
